function clean_string(text)
    new_string = ""
    i = 1
    while i <= sizeof(text)
        character = lowercase(text[i])

        if character in "ãáàâ"
            character = 'a'
        elseif character in "éèê"
            character = 'e'
        elseif character in "íìî"
            character = 'i'
        elseif character in "õóòô"
            character = 'o'
        elseif character in "úùû"
            character = 'u'
        end

        if character in "abcdefghijklmnopqrstuvwxyz"
            new_string = string(new_string, character)
        end

        i = nextind(text, i)
    end
    return new_string
end


function palindromo(string)
    if string == ""
        return true
    end

    text = clean_string(string)
    start_index = 1
    end_index = lastindex(text)
    while start_index <= end_index
        if text[end_index] != text[start_index]
            return false
        end
        start_index = nextind(text, start_index)
        end_index = prevind(text, end_index)
    end
    return true
end


using Test
function test()
    @test ~palindromo("Passei em MAC0110!")
    @test palindromo("A mãe te ama.")
    @test palindromo("Socorram-me, subi no ônibus em Marrocos!")
    @test ~palindromo("MiniEP11")
    @test palindromo("ovo")
    @test palindromo("")
    println("Final dos testes!")
end


test()
